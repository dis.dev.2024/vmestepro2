export default () => {

    if (document.querySelectorAll('[data-field]')) {

        const fields = document.querySelectorAll('[data-field]');

        fields.forEach(elem => {
            const input = elem.querySelector('input');
            console.log(input.value);
            if (input.value) {
                elem.classList.add('field--filled')
            }

            input.oninput = function() {
                if (input.value) {
                    elem.classList.add('field--filled')
                }
                else {
                    elem.classList.remove('field--filled')
                }
            };
        });
    }

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-field-clear]')) {
            event.target.closest('[data-field]').querySelector('input').value = '';
            event.target.closest('[data-field]').classList.remove('field--filled');
        }
    })
};
