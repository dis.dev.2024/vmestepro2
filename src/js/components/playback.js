export default () => {

    const players = document.querySelectorAll('[data-audio]')

    function calculateTotalValue(length) {
        let minutes = Math.floor(length / 60),
            seconds_int = length - minutes * 60,
            seconds_str = seconds_int.toString(),
            seconds = seconds_str.substr(0, 2),
            time = minutes + ':' + parseInt(seconds)

        return time;
    }

    function calculateCurrentValue(currentTime) {
        let current_hour = parseInt(currentTime / 3600) % 24,
            current_minute = parseInt(currentTime / 60) % 60,
            current_seconds_long = currentTime % 60,
            current_seconds = current_seconds_long.toFixed(),
            current_time = (current_minute < 10 ? "0" + current_minute : current_minute) + ":" + (current_seconds < 10 ? "0" + current_seconds : current_seconds);

        return current_time;
    }



    if (players.length > 0) {
        players.forEach(elem => {

            let audioControl = elem.querySelector('[data-audio-control]')
            let audioSource = elem.querySelector('[data-audio-source]')
            let audioProgress = elem.querySelector('[data-audio-progress]')
            let audioDuration = elem.querySelector('[data-audio-duration]')
            let audioCurrent = elem.querySelector('[data-audio-current]')
            let progressEl


            audioControl.addEventListener('click',videoAct);

            audioSource.onloadedmetadata = function() {
                audioCurrent.innerText = calculateTotalValue(audioSource.currentTime);
                audioDuration.innerText = calculateTotalValue(audioSource.duration);
            };

            audioSource.addEventListener("timeupdate", () => {
                progressEl = audioSource.currentTime / audioSource.duration * 100;
                audioCurrent.innerText = calculateTotalValue(audioSource.currentTime);
                audioProgress.style.width = progressEl + '%';
                if (audioSource.currentTime === audioSource.duration) {
                    audioSource.pause();
                    audioSource.currentTime = 0.0;
                    audioControl.classList.remove('play')
                }
            });


            function videoAct() { //Запускаем или ставим на паузу
                if(audioSource.paused) {
                    audioSource.play();
                    audioControl.classList.toggle('play')
                } else {
                    audioSource.pause();
                    audioControl.classList.toggle('play')
                }
            }
        });
    }
};
