"use strict"

import * as usefulFunctions from "./components/functions.js"; // Полезные функции
import maskInput from './forms/input-mask.js'; // Маска ввода для форм
import mobileNav from './components/mobile-nav.js';  // Мобильное меню
import collapse from './components/collapse.js'; // Сворачиваемые блоки
import tabs from './components/tabs.js'; // Tabs
import Rating from './components/rating.js'; // Rating plugin
import quantity from './forms/quantity.js' // input number
import Spoilers from "./components/spoilers.js";
import Dropdown from "./components/dropdown.js";
import Select from './components/select.js' // Select
import Playback from "./components/playback.js";
import InputFields from "./forms/input-fields.js";


import dynamicTextarea from "dynamic-textarea";
import { Fancybox } from "@fancyapps/ui"; // Fancybox modal gallery

// Проверка поддержки webp
usefulFunctions.isWebp();

// Добавление класса после загрузки страницы
usefulFunctions.addLoadedClass();

// Добавление класса touch для мобильных
usefulFunctions.addTouchClass()

// Mobile 100vh
usefulFunctions.fullVHfix();

// Плавный скролл
usefulFunctions.SmoothScroll('[data-anchor]')

// Вкладки (tabs)
tabs();

// Сворачиваемые блоки
collapse();

// Маска для ввода номера телефона
maskInput('input[name="phone"]');

// input Number
quantity()

// Меню для мобильной версии
mobileNav();

// Spoilers
Spoilers();

// Dropdown
Dropdown();

// Rating
Rating();

// Select Dropdown
Select()



const answerField = () => {
    const fields = document.querySelectorAll('[data-answer-field]')

    fields.forEach(elem => {
        console.log(elem.value)
        if(elem.value) {
            elem.closest('[data-answer]').classList.add('answer--focus')
        }

        elem.onfocus = function () {
            elem.closest('[data-answer]').classList.add('answer--focus')
        }
        elem.onblur  = function () {
            if(!elem.value) {
                elem.closest('[data-answer]').classList.remove('answer--focus')
            }
        }
    });
}

document.addEventListener('click', (event) => {
    if (event.target.closest('[data-video-toggle]')) {

        const videoEl = event.target.closest('[data-video]').querySelector('[data-video-source]')

        event.target.closest('[data-video]').classList.toggle('task-video--play')
        if (videoEl.paused) {
            videoEl.play();
        } else {
            videoEl.pause();
        }
    }
})

// Modal Fancybox
Fancybox.bind("[data-fancybox]", {
    autoFocus: false
});

const circleBar = () => {
    if (document.querySelector('[data-circle-counter]')) {
        const counter = document.querySelector('[data-circle-counter]')
        const circle = counter.querySelector('[data-circle-bar]')
        const radius = circle.r.baseVal.value;
        const circumference = 2 * Math.PI * radius;

        circle.style.strokeDasharray = `${circumference} ${circumference}`;
        circle.style.strokeDashoffset = circumference;

        function startTimer(duration){
            let timeLeft = parseInt(duration);
            let percent;
            let downloadTimer = setInterval(function(){
                if(timeLeft <= 0){
                    clearInterval(downloadTimer);
                }
                percent = parseInt(timeLeft * 100 / parseInt(duration));
                setProgress(percent)
                timeLeft -= 1;
            }, 1000);
        }

        function setProgress(value) {
            const offset = circumference - (value / 100) * circumference; // Вычисление значения промежутка
            circle.style.strokeDashoffset = offset; // Установка промежутка от начального значения промежутка
        }
        startTimer(100)
    }
}




// Sliders
import "./components/sliders.js";

window.addEventListener("DOMContentLoaded", function (e) {
    answerField()

    Playback()

    circleBar()

    InputFields()
});

